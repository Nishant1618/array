function filter(elements,cb) {
    if (!elements){
        return []
    }
    let newarray = [];
    for (let i =0; i<elements.length; i++) {
      if (cb(elements[i],i,elements) === true) {
        newarray.push(elements[i]);
      }
    }return newarray;
  }

module.exports = filter;
