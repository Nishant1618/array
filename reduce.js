function reduce(array,cb,startingvalue){
    let current = startingvalue;
    for (let i=0; i<array.length;i++){
         current = cb(current,array[i],i,array);
    }return current
}

module .exports = reduce;
