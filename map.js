function map(items,cb){
    let newarray = [];
    for (let i=0;i<items.length;i++){
        newarray.push(cb(items[i],i,items));
    }return newarray;
}

module.exports = map;
